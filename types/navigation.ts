import { RouteProp } from '@react-navigation/native';

export enum ScreenName {
  HOME = 'HOME',
  LOGIN = 'LOGIN',
}
export enum StackName {
  AUTHENTICATED_STACK = 'AUTHENTICATED_STACK',
  UNAUTHENTICATED_STACK = 'UNAUTHENTICATED_STACK',
}

export type ScreenParamListType = {
  HOME?: undefined;
  LOGIN?: undefined;
  // SHIPPING_FORM: { selectedFigure: Figure };
  // SUMMARY: { summaryDetails: SummaryDetails };
};

export type HomeRouteProps = RouteProp<ScreenParamListType, ScreenName.HOME>;
// export type ShippingFormRouteProps = RouteProp<ScreenParamListType, ScreenName.SHIPPING_FORM>;
// export type SummaryRouteProps = RouteProp<ScreenParamListType, ScreenName.SUMMARY>;

export interface ApiServiceResult<T> {
  success: boolean;
  result: T;
  message?: string;
}

export type Method = 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';

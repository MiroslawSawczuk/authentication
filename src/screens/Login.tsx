import React, { useContext, useState } from 'react';
import { Alert, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { ScreenName } from '../../types/navigation';
import { AuthContext } from '../context/AuthContext';
import { loginUser } from '../services/api/login';
import { useAppNavigation } from '../services/hooks/customHooks';

const Login = () => {
  const navigation = useAppNavigation();
  const { login: loginUserToStore } = useContext(AuthContext);
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  const btnDisabled = !login || !password;
  const styles = createStyle(btnDisabled);

  const handleLogin = async () => {
    const loginResponse = await loginUser(login, password);
    if (loginResponse.success) {
      loginUserToStore();
      navigation.navigate(ScreenName.HOME);
    } else {
      Alert.alert('Invalid email or password');
    }
  };

  return (
    <View style={styles.cosntainer}>
      <TextInput style={styles.input} placeholder="login" value={login} onChangeText={setLogin} />
      <TextInput
        style={styles.input}
        placeholder="Password"
        value={password}
        onChangeText={setPassword}
        secureTextEntry
      />
      <TouchableOpacity style={styles.button} disabled={!login || !password} onPress={handleLogin}>
        <Text>Log in</Text>
      </TouchableOpacity>
    </View>
  );
};

const createStyle = (btnDisabled: boolean) => {
  return StyleSheet.create({
    cosntainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    input: {
      width: '80%',
      height: 40,
      borderWidth: 1,
      borderColor: '#535353',
      borderRadius: 8,
      padding: 8,
      marginBottom: 8,
    },
    button: {
      width: '30%',
      height: 40,
      borderWidth: 1,
      borderColor: '#535353',
      borderRadius: 8,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: btnDisabled ? '#d7d7d7' : '#65c470',
    },
  });
};

export default Login;

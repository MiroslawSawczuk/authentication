import React, { useContext } from 'react';
import { Button, Text, View } from 'react-native';
import { AuthContext } from '../context/AuthContext';

const Home = () => {
  const { isLoggedIn, logout } = useContext(AuthContext);

  const handleLogout = () => {
    logout();
  };

  return (
    <View>
      <Text>Home screen</Text>
      <Text>{isLoggedIn && 'Logged out'}</Text>
      {isLoggedIn && <Button title="Logout" onPress={handleLogout} />}
    </View>
  );
};

export default Home;

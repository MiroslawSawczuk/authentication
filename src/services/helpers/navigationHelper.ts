import { ScreenName } from '../../../types/navigation';
import { navigationRef } from '../../navigation/NavigationStack';
import { clearTokens } from './secureStoreHelper';

export const redirectToLogin = () => {
  clearTokens();
  navigationRef.current?.navigate(ScreenName.LOGIN);
};

import EncryptedStorage from 'react-native-encrypted-storage';

export const setItem = async (key: string, value: string) => await EncryptedStorage.setItem(key, value);
export const getItem = async (key: string) => await EncryptedStorage.getItem(key);
export const removeItem = async (key: string) => await EncryptedStorage.removeItem(key);

export const clearTokens = async () => {
  await removeItem('access_token');
  await removeItem('refresh_token');
};

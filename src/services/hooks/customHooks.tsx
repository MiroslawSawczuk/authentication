import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { ScreenParamListType } from '../../../types/navigation';

export const useAppNavigation = () => useNavigation<StackNavigationProp<ScreenParamListType>>();

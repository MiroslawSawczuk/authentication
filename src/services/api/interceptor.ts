import { Method } from '../../../types/api';
import { redirectToLogin } from '../helpers/navigationHelper';
import { getItem, setItem } from '../helpers/secureStoreHelper';

const BASE_URL = 'https://api.example.com';

export const api = async (method: Method = 'GET', url: string, payload: {}) => {
  try {
    const response = await createRequest(method, url, payload);

    if (response.ok) {
      const data = await response.json();
      return data;
    } else {
      const refreshTokenResponse = await refreshToken();

      if (refreshTokenResponse.ok) {
        const data = await refreshTokenResponse.json();
        await setItem('access_token', data.access_token);
        const retryResponse = await createRequest(method, url, payload);

        if (retryResponse.ok) {
          const data = await retryResponse.json();
          return data;
        } else {
          redirectToLogin();
          throw new Error('Cannot make api call with refreshed accessToken');
        }
      } else {
        redirectToLogin();
        throw new Error('Cannot refresh accessToken using refreshToken.');
      }
    }
  } catch (error) {
    console.error(error);
    throw new Error('Something went wrong');
  }
};

const createRequest = async (method: Method, url: string, body: any): Promise<Response> => {
  const accessToken = await getItem('access_token');
  const response = await fetch(url, {
    method,
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
    body: JSON.stringify({
      body,
    }),
  });

  if (response.status === 401) {
    redirectToLogin();
    throw new Error('Unauthorized');
  }

  return response;
};
const refreshToken = async (): Promise<Response> => {
  const refreshToken = await getItem('refresh_token');
  const response = await fetch(`${BASE_URL}/refresh`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      refresh_token: refreshToken,
    }),
  });

  return response;
};

import { ApiServiceResult } from '../../../types/api';
import { api } from './interceptor';

const BASE_URL = 'https://api.example.com';

export const loginUser = async (login: string, password: string) => {
  let result: ApiServiceResult<boolean> = {
    success: false,
    result: false,
  };

  try {
    const url = `${BASE_URL}/loginUser`;
    const payload = { login, password };
    const responseData: boolean = await api('POST', url, payload);

    if (responseData === true) {
      result = { success: true, result: responseData };
    } else {
      result = { success: false, result: responseData, message: 'Cannot login user. Provide correct credentials' };
    }

    return result;
  } catch (error) {
    console.error(error);
    throw new Error('Something went wrong');
  }
};

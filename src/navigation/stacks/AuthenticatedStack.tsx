import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { ScreenName } from '../../../types/navigation';
import Home from '../../screens/Home';

const Stack = createStackNavigator();
const options = { headerShown: false, gestureEnabled: true };

const AuthenticatedStack = () => {
  return (
    <Stack.Navigator initialRouteName={ScreenName.HOME} screenOptions={options}>
      <Stack.Screen name={ScreenName.HOME} component={Home} />
    </Stack.Navigator>
  );
};

export default AuthenticatedStack;

import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { ScreenName } from '../../../types/navigation';
import Login from '../../screens/Login';

const Stack = createStackNavigator();
const options = { headerShown: false };

const UnauthenticatedStack = () => {
  return (
    <Stack.Navigator initialRouteName={ScreenName.LOGIN} screenOptions={options}>
      <Stack.Screen name={ScreenName.LOGIN} component={Login} />
    </Stack.Navigator>
  );
};

export default UnauthenticatedStack;

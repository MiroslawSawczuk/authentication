import { NavigationContainer, NavigationContainerRef } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React, { useContext } from 'react';
import { ScreenName, StackName } from '../../types/navigation';
import { AuthContext } from '../context/AuthContext';
import AuthenticatedStack from './stacks/AuthenticatedStack';
import UnauthenticatedStack from './stacks/UnauthenticatedStack';

const Stack = createStackNavigator();
export const navigationRef = React.createRef<NavigationContainerRef<any>>();

const NavigationStack = () => {
  const { isLoggedIn } = useContext(AuthContext);
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator initialRouteName={ScreenName.HOME} screenOptions={{ headerShown: false }}>
        {isLoggedIn ? (
          <Stack.Screen name={StackName.AUTHENTICATED_STACK} component={AuthenticatedStack} />
        ) : (
          <Stack.Screen name={StackName.UNAUTHENTICATED_STACK} component={UnauthenticatedStack} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default NavigationStack;

import React from 'react';
import { AuthProvider } from './src/context/AuthContext';
import NavigationStack from './src/navigation/NavigationStack';

const App = () => {
  return (
    <AuthProvider>
      <NavigationStack />
    </AuthProvider>
  );
};

export default App;
